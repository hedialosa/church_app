require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Pefa Kayole B"
    assert_equal full_title("Help"), "Help | Pefa Kayole B"
  end
end

require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "Pefa Kayole B"
  end

 # test "should get root" do
  #  get root_url
   # assert_response :success
  # end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | #{@base_title}"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"
  end

  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end

  test "should get ministries" do
    get ministries_path
    assert_response :success
    assert_select "title", "Ministries | #{@base_title}"
  end

  test "should get departments" do
    get departments_path
    assert_response :success
    assert_select "title", "Departments | #{@base_title}"
  end

  test "should get events" do
      get events_path
      assert_response :success
      assert_select "title", "Events | #{@base_title}"
  end

  test "should get news" do
      get news_path
      assert_response :success
      assert_select "title", "News | #{@base_title}"
  end

  test "should get blog" do
    get blog_path
    assert_response :success
    assert_select "title", "Blog | #{@base_title}"
  end
end

Rails.application.routes.draw do
  root 'static_pages#home'
  get  '/help',        to: 'static_pages#help'
  get  '/about',       to: 'static_pages#about'
  get  '/contact',     to: 'static_pages#contact'
  get  '/ministries',  to: 'static_pages#ministries'
  get  '/departments', to: 'static_pages#departments'
  get  '/events',      to: 'static_pages#events'
  get  '/news',        to: 'static_pages#news'
  get  '/blog',        to: 'static_pages#blog'
  get  '/signup',      to: 'members#new'
end

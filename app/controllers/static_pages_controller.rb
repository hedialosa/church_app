class StaticPagesController < ApplicationController
  def home
  end

  def help
  end

  def about
  end

  def contact
  end

  def ministries
  end

  def departments
  end

  def events
  end

  def news
  end

  def blog
  end
end

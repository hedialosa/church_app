# Church Application

This the first app in my 12-in-12 challange where am building 
12 applications in 12 months in order to learn ruby on rails 
in a more practical handson manner.
This first app will be a church web app built specifically for my church
to help it better cement its place on the interwebs with a good website
that will display dynamic content about the church with a few functionalities to enable the church better
manage the resources at its disposal.
